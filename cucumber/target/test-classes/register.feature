Feature: Register functionality

  Scenario Outline: RegisterTests
    Given Navigate to RegisterPage
    When User enters username "<username>" email "<email>" and password "<password>"
    Then User should be redirect to <isValid> page

    Examples: 
       | username | email          | password       | isValid |
     | user3    | user3@test.com | user3@test.com | login   |
     | random   |                |                | login   |
     | user3    |                |   random       | login   |
       | random   |  random        |      random    | dashboard|
     | random   | user3@test.com |      random    | login  |
     |          |                |                | login   |
      |          |    random      |                | login   |
     |          |               |     random      | login   |
