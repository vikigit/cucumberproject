Feature: Login functionality

  Scenario Outline: LoginTests
    Given Navigate to LoginPage
    When User enters username "<username>" and password "<password>"
    Then User should be redirected to <isValid> page

   Examples: 
     | username | password          | isValid   |
     | user1    | testinginjira2022 | dashboard |
     |          | testinginjira2022 | login     |
     | user1    |                   | login     |
     |          |                   | login     |


 # Scenario: Verify "Lost your password?" is available link
  #  Given Navigate to LoginPage
 #   When User click on "Lost your password?" link
  #  Then User should be redirected to a page for reseting password
