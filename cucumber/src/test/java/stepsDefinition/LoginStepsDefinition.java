package stepsDefinition;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import objects.DashboardPage;
import objects.Header;
import objects.Hooks;
import objects.LoginPage;

public class LoginStepsDefinition {

	public WebDriver driver = new ChromeDriver();
	public LoginPage loginPageObj = new LoginPage(driver);
		public Hooks hooksObject = new Hooks(driver);

	public DashboardPage dashPageObj = new DashboardPage(driver);
	public Header headerObj = new Header(driver);
	
	

	@Given("Navigate to LoginPage")
	public void navigateToLogin() throws InterruptedException {
		hooksObject.testSetUp();
		
		headerObj.clickLogin_RegisterLinkIcon();
		//driver.findElement(By.xpath("//a[@href='http://qacourse.churlinoski.mk/my-account/']//child::span")).click();		
	}
	

	@When("^User enters username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void userLogin(String arg1, String arg2) {
		loginPageObj.loginUser(arg1, arg2);
		
	}
	

	@Then("^User should be redirected to (.+?) page$")
	public void validatePageForLogin(String arg1) throws InterruptedException{
		if(arg1.equals("dashboard")) {
		Boolean isDashboard = dashPageObj.verifyDashboardLinkLeftMenu();
		assertTrue(isDashboard);
		}
		else if(arg1.equals("login")) {
			Boolean isLogin = loginPageObj.verifyLoginButton();
			assertTrue(isLogin);
			
		}
		hooksObject.tesTearDown();
	}
}

/*
	

	@Then("Error allert should be shown")
	public void errorAllert() throws InterruptedException {
		Boolean isDisplayed = driver.findElement(By.xpath("//ul[@class='woocommerce-error']")).isDisplayed();
		assertTrue(isDisplayed);
		hooksObj.tesTearDown();
	}

	
	@Then("Error allert should be displayed")
	public void usernameIsRequired() throws InterruptedException {
	  Boolean  error = driver.findElement(By.xpath("//li//strong")).isDisplayed();
	   
	    assertTrue(error);  
	    hooksObj.tesTearDown();
	}

	

	@When("User click on \"Lost your password?\" link")
	public void clickOnLostYourPasswordLink() {
		loginPageObj.clickLostYourPasswordLinkText();
		
	}
	@Then("User should be redirected to a page for reseting password")
	public void verifyUserIsOnResetingPasswordPage() throws InterruptedException {
		 Boolean resetPasswordButton =  driver.findElement(By.xpath("//button[text()=\"Reset password\"]")).isDisplayed();
		    assertTrue(resetPasswordButton);
		    
	}
	*/
	//@After
	//public void tesTearDown() throws InterruptedException {
	//	Thread.sleep(5000);
	//	driver.quit();
		
	