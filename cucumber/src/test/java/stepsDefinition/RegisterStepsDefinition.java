package stepsDefinition;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import objects.DashboardPage;
import objects.Header;
import objects.Hooks;
import objects.RegisterPage;

public class RegisterStepsDefinition {
	
	public WebDriver driver = new ChromeDriver();
	public RegisterPage registerPageObj = new RegisterPage(driver);
	public DashboardPage dashPageObj = new DashboardPage(driver);
	public Header headerObj = new Header(driver);
	public Hooks hooksObj = new Hooks(driver);

	
	@Given("Navigate to RegisterPage")
	public void navigateToRegister() throws InterruptedException {
		
		hooksObj.testSetUp();
		headerObj.clickLogin_RegisterLinkIcon();
		
	}

	@When("^User enters username \"([^\"]*)\" email \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void registerUser(String arg1, String arg2, String arg3) {
		Random randomGen = new Random();
		
		int rand = randomGen.nextInt(100000);
		if(arg1.equals("random")){
			arg1 = "userlogin" + Integer.toString(rand);
		}
	   else if (arg2.equals("random")) {
			arg2 = arg1 + "@loguser.com";
		}
	   else if(arg3.equals("random")){
		   arg3 = RandomStringUtils.randomAlphabetic(8) + rand;
			
		}
		registerPageObj.registerAccount(arg1, arg2, arg3);
		
	}
	
	@Then("^User should be redirect to (.+?) page$")
	public void validatePageForRegister(String arg1) throws InterruptedException{
		if(arg1.equals("dashboard")) {
		Boolean isDashboard = dashPageObj.verifyDashboardLinkLeftMenu();
		assertTrue(isDashboard);
		}
		else if(arg1.equals("login")) {
			Boolean isLogin = registerPageObj.verifyRegisterButton();
			assertTrue(isLogin);
			
		}
	hooksObj.tesTearDown();
}
}
