package objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header {
private WebDriver driver;
	

	@FindBy(xpath = "//a[text()='Training Store']")
	WebElement titleOfThePage;
	
	@FindBy(xpath = "//span[@class='site-description']")
	WebElement siteDescription;
	
	/*
	@FindBy(xpath = "//div[@class=\"site-tools\"]//descendant::span[@class=\"woostify-svg-icon icon-user\"]")
	WebElement accountLinkButton;
	*/
	
	@FindBy(xpath = "//span[text()='Home']//ancestor::ul[@id='menu-main']")
	WebElement homeLinkText;
	
	@FindBy(xpath = "(//a//child::span[text()='Products'])[1]")
	WebElement productsLinkText;
	
	@FindBy(xpath = "(//a//child::span[text()='Clothing'])[1]")
	WebElement clothingProductsSubMenu;
	
	@FindBy(xpath = "(//a//child::span[text()='Decor'])[1]")
	WebElement decorProductsSubMenu;
	
	@FindBy(xpath = "(//a//child::span[text()='Music'])[1]")
	WebElement musicProductsSubMenu;
	
	@FindBy(xpath = "(//a//child::span[text()='Tshirts'])[1]")
	WebElement tshirtsProductsSubMenu;
	
	@FindBy(xpath = "(//a//child::span[text()='Hoodies'])[1]")
	WebElement hoodiesProductsSubMenu;
	
	@FindBy(xpath = "(//a//child::span[text()='Accessories'])[1]")
	WebElement accessoriesProductsSubMenu;
	
	
	@FindBy(xpath = "(//span[text()='About'])[1]")
	WebElement aboutLinkText;
	
	@FindBy(xpath = "(//span[text()='Contact'])[1]")
	WebElement contactLinkText;
	
	@FindBy(xpath = "//span[@class='woostify-svg-icon icon-search']//ancestor::div[@class='site-tools']")
	WebElement searchLinkIcon;
	
	@FindBy(xpath = "//span[@class='woostify-svg-icon icon-user']")
	WebElement logIn_RegisterIcon;
	
	@FindBy(xpath = "//a[@class='tools-icon shopping-bag-button shopping-cart']")
	WebElement shoppingCartIcon;
	
	
	
	
	
	
	//initializing the Page Objects
	public Header(WebDriver webDriver) {
		this.driver = webDriver;
		PageFactory.initElements(driver, this);
	}
	
	//actions
	public String getTitleOfThePage() {
		return titleOfThePage.getText();
		
	}
	
	public String getDescriptionOfThePage() {
		return siteDescription.getText();
		
	}
	
	/*
	public Header clickAccountLinkButton() {
		accountLinkButton.click();
		return this;
	}
	*/
	
	public Header clickHomeLinkText() {
		homeLinkText.click();
		return this;
	}
	public Header clickProductsLinkText() {
		productsLinkText.click();
		return this;
	}
	
	
	
	public Header clickAboutLinkText() {
		aboutLinkText.click();
		return this;
	}
	public Header clickContactLinkText() {
		contactLinkText.click();
		return this;
	}
	public Header clickSearchLinkIcon() {
		searchLinkIcon.click();
		return this;
	}
	public Header clickLogin_RegisterLinkIcon() {
		logIn_RegisterIcon.click();
		return this;
	}
	public Header shoppingCartLinkIcon() {
		shoppingCartIcon.click();
		return this;
	}
	public void clickOnClothingProductsSubMenu() {
		Actions action = new Actions(driver);
		action.moveToElement(productsLinkText).build().perform();
		clothingProductsSubMenu.click();
			
	}
	public void clickOnDecorProductsSubMenu() {
		Actions action = new Actions(driver);
		action.moveToElement(productsLinkText).build().perform();
		decorProductsSubMenu.click();
			
	}
	public void clickOnMusicProductsSubMenu() {
		Actions action = new Actions(driver);
		action.moveToElement(productsLinkText).build().perform();
		musicProductsSubMenu.click();
			
	}
	
	public void clickOnTshirtsProductsSubMenu() {
		Actions action = new Actions(driver);
		action.moveToElement(productsLinkText).build().perform();
		tshirtsProductsSubMenu.click();
			
	}
	public void clickOnHoodiesProductsSubMenu() {
		Actions action = new Actions(driver);
		action.moveToElement(productsLinkText).build().perform();
		hoodiesProductsSubMenu.click();
			
	}
	public void clickOnAccessoriesProductsSubMenu() {
		Actions action = new Actions(driver);
		action.moveToElement(productsLinkText).build().perform();
		accessoriesProductsSubMenu.click();
			
	}

}
