package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Hooks {
	private WebDriver driver;    // = new ChromeDriver();

	@Before
	public void testSetUp() throws InterruptedException {
		WebDriverManager.chromedriver().setup();

		//System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver.get("http://qacourse.churlinoski.mk/");
		
		driver.findElement(By.xpath("//*[@class=\"woocommerce-store-notice__dismiss-link\"]")).click();
	
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
	}
	
	@After
	public void tesTearDown() throws InterruptedException{
		Thread.sleep(5000);
		driver.quit();
	
	
		
	}
	public Hooks(WebDriver webDriver) {
		this.driver = webDriver;
	}
	
}

