package objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {
	private WebDriver driver;

	@FindBy(xpath = "//a//child::span[@class=\"woostify-svg-icon icon-dashboard\"]")
	WebElement iconDashboardLinkLeftMenu;

	public DashboardPage(WebDriver webDriver) {
		this.driver = webDriver;
		PageFactory.initElements(driver, this);
	}

	public boolean verifyDashboardLinkLeftMenu() {
		return iconDashboardLinkLeftMenu.isDisplayed();
	}

}
