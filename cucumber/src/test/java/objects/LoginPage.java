package objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	 public WebDriver driver;
	

	   @FindBy(xpath = "//input[@id='username']")
		WebElement loginUsernameOrEmailAddressInput;

		@FindBy(xpath = "//label[@for='username']/span")
		WebElement redStarLoginUserOrEmail;

		@FindBy(xpath = "//label[@for='password']/span")
		WebElement redStarLoginPassword;

		@FindBy(xpath = "//input[@id='password']")
		WebElement loginPasswordInput;
		
		@FindBy(xpath = "//input[@id='rememberme']")
		WebElement rememberMeCheckBox;

		@FindBy(xpath = "//button[@name='login']")
		WebElement loginButton;

		@FindBy(xpath = "//a[text()='Lost your password?']")
		WebElement lostYourPasswordLinkText;
	
		public LoginPage(WebDriver webDriver) {
			this.driver = webDriver;
			PageFactory.initElements(driver, this);

		}
	
	public LoginPage setUsernameOrEmailInput(String username) {
		loginUsernameOrEmailAddressInput.sendKeys(username);
		return this;

	}

	public LoginPage setPasswordInput(String password) {
		loginPasswordInput.sendKeys(password);
		return this;

	}

	public LoginPage clickRemembermeCheckBox() {
		rememberMeCheckBox.click();
		return this;
	}

	public LoginPage clickLoginButton() {
		loginButton.click();
		return this;
	}

	public LoginPage clickLostYourPasswordLinkText() {
		lostYourPasswordLinkText.click();
		return this;
	}

	

//verify that Elements on Login page are available

	public boolean verifyUsernameOrEmailInput() {
		return loginUsernameOrEmailAddressInput.isDisplayed();
	}

	public boolean verifyPasswordInput() {
		return loginPasswordInput.isDisplayed();
	}

	public boolean verifyRedStarLoginUserOrEmail() {
		return redStarLoginUserOrEmail.isDisplayed();
	}

	public boolean verifyRedStarLoginPassword() {
		return redStarLoginPassword.isDisplayed();
	}

	

	public boolean verifyLoginButton() {
		return loginButton.isDisplayed();
	}

	public boolean verifyRemembermeIsDisplayed() {
		return rememberMeCheckBox.isDisplayed();
	}
	
	//common function
	public void loginUser(String username, String password) {
		loginUsernameOrEmailAddressInput.sendKeys(username);
		loginPasswordInput.sendKeys(password);
		loginButton.click();
	}
	
}
