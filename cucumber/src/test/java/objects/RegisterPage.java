package objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//input[@id='reg_username']")
	WebElement registerUsernameInput;

	@FindBy(xpath = "//label[@for='username']/span")
	WebElement redStarRegUsername;

	@FindBy(xpath = "//input[@id='reg_email']")
	WebElement registerEmailInput;

	@FindBy(xpath = "//label[@for='reg_email']/span")
	WebElement redStarRegEmail;

	@FindBy(xpath = "//input[@id='reg_password']")
	WebElement registerPasswordInput;

	@FindBy(xpath = "//label[@for='reg_password']/span")
	WebElement redStarRegPassword;

	@FindBy(xpath = "//span[@class='show-password-input']//preceding-sibling::input[@id='reg_password']")
	WebElement eyeIconReg;

	@FindBy(xpath = "//p//child::a[text()='privacy policy']")
	WebElement textUnderPassword;
	
	@FindBy(xpath = "//a[text()='privacy policy']")
	WebElement privacyPolicyLink;
	
	@FindBy(xpath = "//button[text()='Register']")
	WebElement registerButton;
	
	public RegisterPage(WebDriver webDriver) {
		this.driver = webDriver;
		PageFactory.initElements(driver, this);

	}
	public RegisterPage setTextRegisterUsernameInput(String username) {
		registerUsernameInput.sendKeys(username);
		return this;

	}

	public RegisterPage setTextRegisterEmailInput(String email) {
		registerEmailInput.sendKeys(email);
		return this;
	}

	public RegisterPage setTextRegisterPassword(String password) {
		registerPasswordInput.sendKeys(password);
		return this;
	}

	public RegisterPage clickPrivacyPolicy() {
		privacyPolicyLink.click();
		return this;
	}
	public RegisterPage clickRegisterButton() {
		registerButton.click();
		return this;
	}

	public RegisterPage clickEyeRegIcon() {
		eyeIconReg.click();
		return this;
	}

	// common function
	public void registerAccount(String username, String email, String password) {
		
		registerUsernameInput.sendKeys(username);
		registerEmailInput.sendKeys(email);
		registerPasswordInput.sendKeys(password);
		registerButton.click();

	}

	public boolean verifyredStarRegUsername() {
		return redStarRegUsername.isDisplayed();
	}

	public boolean verifyredStarRegEmail() {
		return redStarRegEmail.isDisplayed();
	}
	
	public boolean verifyTextUnderPassword() {
		return textUnderPassword.isDisplayed();
	}
	public boolean verifyredStarRegPassword() {
		return redStarRegPassword.isDisplayed();
	}

	public boolean verifyredEyeIcon() {
		return eyeIconReg.isDisplayed();
	}
	public boolean verifyRegisterButton() {
		return registerButton.isEnabled();
	}
}


